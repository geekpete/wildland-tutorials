# Introduction to Forest creation

## What?

The Wildland Forest proposes a way of organizing your manifests so that you can share them with the world. Keep in mind that you can still use the Forest in a single-user scenario the same way you would use it in a group scenario.

## How?

Before you start, make sure that you have an existing storage where your Forest is going to be bootstrapped. This can also be your local filesystem. This tutorial will use a remote, single S3 bucket to store both manifests and actual data.

1. Create storage Templates for your storage backend.

    _NB: In the example below, we create both `read-write` and `read-only` templates to allow setting different `--access` for each storage type. Note that the `--manifest-pattern` is required for the storage template which will be used to store **manifest files** (also known as a **manifest-catalog**). The example below will use the same storage templates for both \_manifests_ and _data_ thus both templates will refer to the same bucket.\_

    ```console
    $ wl storage-template create s3 --s3-url s3://<bucket-name> --manifest-pattern "/{path}.yaml" --access-key <redacted> --secret-key <redacted> --access bob demo-s3
    ```

    ```console
    $ wl storage-template add s3 --s3-url s3://<bucket-name> --manifest-pattern "/{path}.yaml" --access-key <redacted> --secret-key <redacted> --access '*' --read-only demo-s3
    ```

1. Create and bootstrap the Forest:

    _fixme: users infrastructure should have only r/o storage https://gitlab.com/wildland/wildland-client/-/issues/339_

    ```console
    bob@wildland-client:~$ wl forest create bob demo-s3
    Created base path: /.manifests/a971e384-43a1-4386-9c61-7b2ee695b17d
    Adding storage 3be016e1-3b95-42b8-b162-104316310cef to container.
    Saved container /home/user/.config/wildland/containers/bob-forest-catalog.1.container.yaml
    Adding storage 0fe189ae-3c26-43a2-8923-0b2ca7cc8c6d to container.
    Saved container /home/user/.config/wildland/containers/bob-forest-catalog.1.container.yaml
    Saved: /home/user/.config/wildland/users/bob.user.yaml
    Saved: /home/user/.config/wildland/users/bob.user.yaml
    ```

    The result of this command is an S3 storage backend with the following directory tree:

    ```
    /.manifests/a971e384-43a1-4386-9c61-7b2ee695b17d/
    ├── .manifests.yaml (user's manifest-catalog)
    ├── .uuid
    │   └── a971e384-43a1-4386-9c61-7b2ee695b17d.yaml (user's manifest-catalog, /.uuid/ path)
    └── forest-owner.yaml (user's manifest)
    ```

1. Create a container to place in your forest:

    ```console
    bob@wildland-client:~$ wl container create hello-world --path /hello/world
    ```

1. Add some storage to it:

    ```console
    bob@wildland-client:~$ wl storage create static --file "Hello.md=Hello World!" --container hello-world
    ```

    The container should automatically re-publish, resulting in the following, updated directory tree:

    ```
    /.manifests/a971e384-43a1-4386-9c61-7b2ee695b17d/
    ├── .manifests.yaml
    ├── .uuid
    │   ├── 4298d1e0-4567-495f-8d53-77b40e0ebf56.yaml
    │   └── a971e384-43a1-4386-9c61-7b2ee695b17d.yaml
    ├── forest-owner.yaml
    └── hello
        └── world.yaml
    ```

1. Mount the container you've just published using the Wildland path to your forest which you can share with anyone who has access to your Forest.

    _NB: Remember that before you share your Wildland path with someone, the recipient will likely have to import your user manifest first using the `wl user import` command._

    ```console
    $ wl c mount wildland:0xbob:/hello/world:

    Loading containers. Loaded 1...
    Mounting 1 container

    bob@wildland-client:~$ tree ~/wildland
    /home/user/wildland
    └── hello
        └── world
            └── Hello.md

    2 directories, 1 file

    bob@wildland-client:~$ cat ~/wildland/hello/world/Hello.md
    Hello World!
    ```

    _fixme: The command below doesn't work yet https://gitlab.com/wildland/wildland-client/-/issues/443_

1. You may also create a bridge to your local Forest (or any Forest for that matter) to allow custom path(s) to access the Forest as well as ability to refresh user manifests to which the Forest is pointing to

    ```console
    bob@wildland-client:~$ wl bridge create --target bob --path /work/bob bob-bridge
    bob@wildland-client:~$ wl forest mount :/work/bob:
    ```

    An example of creating a bridge to an external Forest (e.g. a publicly available Pandora Forest) can be found in [Quick Start][wl-quickstart]

[wl-quickstart]: ./quick-start.md
