const title = "Wildland documentation";
const description =
  "Wildland is a collection of protocols, conventions, software, and (soon) a marketplace for leasing storage and in the future compute infrastructure.";
const author = "Golem Foundation";
const ogprefix = "og: http://ogp.me/ns#";

module.exports = {
  /**
   * Ref：https://v1.vuepress.vuejs.org/config/#title
   */
  title,
  /**
   * Ref：https://v1.vuepress.vuejs.org/config/#description
   */
  description,

  /**
   * Extra tags to be injected to the page HTML `<head>`
   *
   * ref：https://v1.vuepress.vuejs.org/config/#head
   */
  head: [
    ["meta", { prefix: ogprefix, property: "og:title", content: title }],
    ["meta", { prefix: ogprefix, property: "twitter:title", content: title }],
    ["meta", { prefix: ogprefix, property: "og:type", content: "article" }],
    [
      "meta",
      {
        prefix: ogprefix,
        property: "og:url",
        content: "https://docs.wildland.io",
      },
    ],
    [
      "meta",
      { prefix: ogprefix, property: "og:description", content: description },
    ],
    [
      "meta",
      { prefix: ogprefix, property: "og:article:author", content: author },
    ],
    ["meta", { name: "theme-color", content: "#3eaf7c" }],
    ["meta", { name: "apple-mobile-web-app-capable", content: "yes" }],
    [
      "meta",
      { name: "apple-mobile-web-app-status-bar-style", content: "black" },
    ],
    [
      "link",
      { rel: "icon", type: "image/png", sizes: "32x32", href: "/favicon.png" },
    ],
    ["apple-touch-icon", { sizes: "512x512", href: "/favicon-512.png" }],
    ["link", { rel: "preconnect", href: "https://fonts.gstatic.com" }],
    [
      "link",
      {
        rel: "stylesheet",
        href: "https://fonts.googleapis.com/css2?family=Inter:wght@400;500;600;700;800&display=swap",
      },
    ],
    [
      "script",
      {},
      `document.body.addEventListener("click", (e) => {
        if (e.target.classList.contains("logo")) {
          e.preventDefault();
          window.location = "https://wildland.io"
        }
      })`,
    ],
  ],

  /**
   * Theme configuration, here is the default theme configuration for VuePress.
   *
   * ref：https://v1.vuepress.vuejs.org/theme/default-theme-config.html
   */
  themeConfig: {
    repo: "",
    editLinks: false,
    docsDir: "",
    editLinkText: "",
    lastUpdated: false,
    logo: "/logo-mark.svg",
    nav: [
      {
        text: "Download",
        link: "https://gitlab.com/wildland/wildland-client",
      },
    ],
    sidebar: [
      {
        title: "Introduction",
        path: "/",
        sidebarDepth: 1,
        // children: ["/"],
      },
      {
        title: "User guide",
        sidebarDepth: 1,
        children: [
          "user-guide/quick-start",
          "user-guide/public-forests",
          {
            title: "Platform specific",
            path: "/user-guide/platform-specific/",
            collapsable: false,
            sidebarDepth: 0,
            children: [
              ["/user-guide/platform-specific/linux", "Linux"],
              ["/user-guide/platform-specific/macos", "macOS"],
              ["/user-guide/platform-specific/windows", "Windows 10"],
            ],
          },
          "/user-guide/sharing-and-access-control",
          "/user-guide/group-users",
          "/user-guide/encryption-backend",
          // "/user-guide/storage-migration",
          {
            title: "Storage backends",
            path: "/user-guide/storage-backends/",
            collapsable: false,
            sidebarDepth: 0,
            children: [
              ["/user-guide/storage-backends/dropbox", "Dropbox"],
              ["/user-guide/storage-backends/google-drive", "Google Drive"],
              ["/user-guide/storage-backends/s3", "Amazon S3"],
              ["/user-guide/storage-backends/webdav", "WebDAV"],
            ],
          },
        ],
      },
      {
        title: "The Paper ",
        path: "https://golem.foundation/resources/documents/wildland-w2h.pdf",
      },
      {
        title: "Developers Docs ",
        path: "https://wildland.gitlab.io/wildland-client/index.html",
      },
      {
        title: "Mailing List ",
        path: "https://wildland.topicbox.com/groups/users",
      },
      {
        title: "© 2021 Golem Foundation Zug, Switzerland",
      },
    ],
  },

  /**
   * Apply plugins，ref：https://v1.vuepress.vuejs.org/zh/plugin/
   */
  plugins: ["@vuepress/plugin-back-to-top", "@vuepress/plugin-medium-zoom"],

  markdown: {
    extendMarkdown: (md) => {
      md.use(require("markdown-it-footnote"));
    },
  },
};
