# Windows

Windows client is not currently available, but is planned for the future.

To run Wildland on Windows, use its Docker version, following the instructions provided in the [Quick Start Guide][quick-start].

Wildland runs smoothly in Docker. You can expose its content to Windows using WebDAV.

The default address is 127.0.0.1:8080. Check out [this][webdav-guide] guide to see how to use WebDAV on Windows.

You will be able to browse your (directly mounted) containers and their content in the File Explorer, but you will not be able to see the content of forests. This is because semicolons are used in forest paths and Windows is unable to interpret that correctly. This issue will be fixed with the release of Windows client.

[quick-start]: ../quick-start.md
[webdav-guide]: https://uvacollab.screenstepslive.com/s/help/m/sitetools/l/611578-how-do-i-set-up-webdav-for-windows-10-or-8
