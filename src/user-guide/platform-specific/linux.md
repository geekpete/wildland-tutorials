# Linux

## Run Wildland client in a Docker container (recommended)

The most convenient way to use the Wildland client on Linux is via the provided Docker image.

First make sure that you have Docker Engine and Docker Compose installed on your system. If you haven't installed it yet, follow the steps described in the handy tutorials prepared by the Docker team:

- [Install Docker Engine][docker-engine]
- [Install Docker Compose][docker-compose]

By default the Docker daemon runs as the `root` user. To run Docker containers as a non-root user, create a group called `docker` and add your user to it:

1. Create the `docker` group by executing the following command:

    ```console
    $ sudo groupadd docker
    ```

1. Add your user to the `docker` group.

    ```console
    $  sudo usermod -aG docker $USER
    ```

1. Log out and log back in so that the changes from the previous steps take effect.

1. Once you have Docker Engine and Docker Compose installed and properly configured with your system, clone the `stable` branch of the Wildland client from the project's [GitLab repository][gitlab] using `git clone` (if you have never used `git` before please install it by following the instructions available [here][git]):

    ```console
    $ git clone -b stable https://gitlab.com/wildland/wildland-client.git
    ```

1. Next, change the working directory to `wildland-client/docker`:

    ```console
    $ cd wildland-client/docker/
    ```

1. Build the Docker image:

    ```console
    $ docker-compose build
    ```

1. Run it:

    ```console
    $ docker-compose run --service-ports wildland-client
    ```

You can now connect with Wildland using WebDAV and a file manager of your choice (Nautilus, Konqueror, Thunar, Dolphin, etc.) by typing `dav://localhost:8080/` in the address bar.

Now, head over to the [Quick Start Guide][quick-start] and follow the instructions there.

## Run Wildland client directly

1. Clone the `stable` branch of Wildland client from the project's [GitLab repository][gitlab]:

    ```console
    $ git clone -b stable https://gitlab.com/wildland/wildland-client.git
    ```

1. Change your working directory to wildland-client:

    ```console
    $ cd wildland-client
    ```

1. Run Wildland client:

    ```console
    $ ./wildland-docker.sh
    ```

1. Follow the instructions available at [Quick Start Guide][quick-start].

[docker-engine]: https://docs.docker.com/engine/install/
[docker-compose]: https://docs.docker.com/compose/install/
[quick-start]: ../quick-start.md
[gitlab]: https://gitlab.com/wildland
[git]: https://git-scm.com/download/linux
