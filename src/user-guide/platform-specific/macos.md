# MacOS

## Run Wildland client from a Docker image (recommended)

### Install Docker

Using the Homebrew package manager (if you don't have Homebrew, install it following the steps described on their [home
page](https://brew.sh)):

```
% brew install --cask docker
```

Launch the Docker.app and let it finish the installation.

### Clone Wildland repository

```
% git clone -b stable https://gitlab.com/wildland/wildland-client.git
```
### Build and start Docker image

```
% cd wildland-client/docker
% docker-compose build
% docker-compose run --service-ports wildland-client
```

This will start the Wildland terminal session through which you can
interact with Wildland client using the `wl` command.

To connect to the Wildland filesystem using Finder, select _Connect to Server_ from _Go_ menu and type `http://localhost:8080` as
the server address. If you get a warning about an insecure connection, just choose to connect and select "Connect as Guest" when asked
for a password.

Once you have completed all of these steps, head over to the [Quick Start Guide][quick-start] and follow the instructions there.


## Run Wildland macOS client

Wildland macOS client is under active development and has not received a lot of testing yet. Use it only if you are prepared to
encounter some bugs, and willing to risk losing your data. Also, do not expect any real GUI frontend. Currently the only way to interact with Wildland is through the command line interface.

### Cloning

Clone the macOS client from the project's [GitLab repository](https://gitlab.com/wildland/wildland-client.git):

```console
% git clone https://gitlab.com/wildland/wildland-client-macos.git
```

### Building

First of all, make sure that you checked out all the dependencies in source form:

```
git submodule update --init
```

and in binary form:

```
brew install signify-osx autoconf autogen automake
```

This project currently uses the following dependencies:

- Python: cpython 3.9.2
- OpenSSL: 1.1.1i
- xz: 5.2.5
- wildland-client: closely tracking project HEAD
- signify: latest stable version available from HomeBrew

The Xcode project is currently configured to embed all the above-mentioned dependencies into the application bundle.

Python and OpenSSL are built from source, which means that:

- fresh build can take some time,
- if you encounter any problems, the first thing to do is to clean
  the build directory.

Building using Xcode should be straightforward: just open WildandClient xcode project, make sure that _WildlandClient_
target is selected and press Cmd+B.

Building from the command line:

```
cd WildlandClient
xcodebuild -scheme WildlandClient OBJROOT=/tmp/wl-build SYMROOT=/tmp/wl-products
```

should build the project and put the resulting artifacts in `/tmp/wl-products`.

### First launch

Copy _WildlandClient.app_ to your /Applications folder.

You start the client by opening WildlandClient.app. This installs the daemon which will create the mount point for Wildland. The daemon will launch automatically once you log in.

### Using

Wildland's command line interface is available at:

```
/Applications/WildlandClient.app/Contents/MacOS/wl-cli
```

See [Quick Start Guide][quick-start] for more.

[quick-start]: ../quick-start.md
