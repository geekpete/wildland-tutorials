# Dropbox Plugin

This tutorial explains how to use the Wildland Dropbox plugin which allows you to store and manage your files kept in your Dropbox account through Wildland.

Before reading the following tutorial, it is recommended to get familiar with the [fundamentals of Wildland][practical-into-to-wildland].

## Generating a Dropbox access token

The Wildland Dropbox plugin allows you to use Dropbox as read/write storage for Wildland containers. To create Dropbox storage backend, you need to generate a [Dropbox access token][oauth-guide] which will be passed as a `--token` parameter Wildland CLI when creating storage (with `wl storage create`) or storage template (with `wl storage-template create dropbox`).

To generate a Dropbox access token, follow these steps:

1. Navigate to the [Dropbox App Console][dropbox-dev-apps-console] and select `Create app`.

   ![Create app](../../assets/screenshots/dropbox/dropbox-create-app.png)

1. In the `Choose an API` section select `Scoped access` (which is the only option available at the time of writing this tutorial). To give access to all files stored in your Dropbox account, select `Full Dropbox` access. To create a directory which will be dedicated solely to Wildland, use the `App folder` instead. The name of the app is an arbitrary unique name which will be displayed on the [Dropbox App Console][dropbox-dev-apps-console] landing page. Click on `Create app` to submit the selected settings.

   ![Choose an API: Scoped access](../../assets/screenshots/dropbox/dropbox-create-app-initial-settings.png)

1. Navigate to the `Permissions` tab and select the following permissions from the `Files and folders` section:

   - `files.metadata.write`
   - `files.metadata.read`
   - `files.content.write`
   - `files.content.read`

   ![Permissions tab](../../assets/screenshots/dropbox/dropbox-permissions-settings.png)

1. Make sure to apply the changes by clicking on the `Submit` button.

   ![Submit permissions](../../assets/screenshots/dropbox/dropbox-permissions-change-successful.png)

1. Navigate back to the `Settings` tab and select `No expiration` in the `Access token expiration` section (instead of `Short-lived` which is set as the default option).

   ![Settings tab](../../assets/screenshots/dropbox/dropbox-access-token.png)

   Note that `long-lived` tokens are less secure than `short-lived` and will be [deprecated][oauth-guide] by Dropbox in the future. This issue will be addressed in the next Wildland release.

   ![Long lived sessions warning](../../assets/screenshots/dropbox/dropbox-long-lived-sessions.png)

1. Click the `Generate` button under the `Generated access token` section.

   ![Generate access token](../../assets/screenshots/dropbox/dropbox-generate-access-token-button.png)

   Copy the access token to store it somewhere safe and don't share it with anyone. This token will be used to access your Dropbox account from Wildland. Note that after navigating away from the Wildland Console App you will not be able to see the generated token again (you will need to generate another access token in this case). After each modification of the Dropbox permissions (Dropbox `Permissions` tab) you will need to generate a new access token to make the newly selected set of permissions active.

   ![Generate access token](../../assets/screenshots/dropbox/dropbox-generated-access-token.png)

## Creating Dropbox storage

Create a container for your Dropbox storage:

```console
$ wl container create --path /dropbox dropbox-container
```

If you want to create Dropbox storage use the following command:

```console
$ wl template create dropbox \
    --container dropbox-container \
    --token DROPBOX_ACCESS_TOKEN
```

The `--token` parameter specifies your personal Dropbox access token generated as mentioned above.

## Mounting

The mounting process is no different than that of any other container.

```console
$ wl start
$ wl c mount dropbox-container
```

You are now ready to access your Dropbox account using Wildland! To list your files, run:

```console
$ ls -la ~/wildland/dropbox
```

[practical-into-to-wildland]: ../../index.md
[dropbox-dev-apps-console]: https://www.dropbox.com/developers/apps
[oauth-guide]: https://developers.dropbox.com/pl-pl/oauth-guide
