# Supported storage backends

Wildland is not dependent on any particular type of infrastructure, and our end goal is to make it backend-agnostic. At the moment, however, we suggest using the following backends for which there are convenient storage templates and HOWTOs available.

| STORAGE      | HOWTO                  |
| ------------ | ---------------------- |
| Dropbox      | [HOWTO][dropbox-howto] |
| Google Drive | [HOWTO][google-howto]  |
| Amazon S3    | [HOWTO][s3-howto]      |
| WebDAV       | [HOWTO][webdav-howto]  |

To support different types of storage-access protocols Wildland client implements a pluggable architecture. The list of currently supported plugins can be obtained using the `wl template create` command.

```console
$ wl template create
Usage: wl template create [OPTIONS] COMMAND [ARGS]...

  Creates storage template based on storage type.

Options:
  --help  Show this message and exit.

Commands:
  bear-db           Create bear-db storage template
  categorization    Create categorization storage template
  date-proxy        Create date-proxy storage template
  delegate          Create delegate storage template
  dropbox           Create dropbox storage template
  dummy             Create dummy storage template
  encrypted         Create encrypted storage template
  googledrive       Create googledrive storage template
  http              Create http storage template
  imap              Create imap storage template
  ipfs              Create ipfs storage template
  local             Create local storage template
  local-cached      Create local-cached storage template
  local-dir-cached  Create local-dir-cached storage template
  s3                Create s3 storage template
  static            Create static storage template
  webdav            Create webdav storage template
  zip-archive       Create zip-archive storage template

```

To find out more about these plugins, and how you can implement one yourself read the Wildland [Plugins HOWTO][plugins-howto].

[dropbox-howto]: ./dropbox.md
[google-howto]: ./google-drive.md
[s3-howto]: ./s3.md
[webdav-howto]: ./webdav.md
[plugins-howto]: https://wildland.gitlab.io/wildland-client/plugins-howto.html
