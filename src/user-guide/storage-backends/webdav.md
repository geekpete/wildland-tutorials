# WebDAV Plugin

Storage infrastructure which supports WebDAV can be used as a backend for Wildland.

## Create a template

To create a WebDAV template, the user needs the login, password and url of the infrastructure that's to be used. Note that access rights should reflect the intended use of the infrastructure, see the [Access Control and Sharing][access_control] guide for more information.

```console
$ wl template create webdav mywebdav \
    --login <USERNAME> \
    --password <PASSWORD> \
    --url https://<URL>
```

## Fastmail example

Fastmail provides a files cloud storage service which can be accessed through WebDAV. To use it with Wildland, the user needs to generate an app password: 

Fastmail: Settings -> Passwords and Security -> App Passwords (Manage) -> New App Password -> Access: Files (FTP, WebDAV) -> Generate password.

As the login, the user needs to provide their main Fastmail login e-mail, the url is https://myfiles.fastmail.com. 

Fastmail does not provide read-only access, which is why it's best used as a personal storage backend, and not to be shared with others. You can read more about Fastmail Files and WebDAV [here][fastmail_webdav].   

[access_control]: ./sharing-and-access-control.md
[fastmail_webdav]: https://www.fastmail.com/help/files/davnftp.html
