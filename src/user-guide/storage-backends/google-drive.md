# Google Drive Plugin

This tutorial explains how to create Google Drive-based read/write storage backend for Wildland containers.

### Obtaining user credentials

1. Create a new project on the developer console via: [https://console.cloud.google.com/projectcreate](https://console.cloud.google.com/projectcreate).

    ![Create new project](../../assets/screenshots/googledrive/1.png)

1. Select `SELECT PROJECT`.

    ![SELECT PROJECT](../../assets/screenshots/googledrive/2.png)

1. On the project page, select `Go to APIs overview`.

    ![Go to APIs overview](../../assets/screenshots/googledrive/3.png)

1. Click `ENABLE APIS AND SERVICES`.

    ![ENABLE APIS AND SERVICES](../../assets/screenshots/googledrive/4.png)

1. Search for `Google Drive API`, select it and press `Enable`.

    ![Google Drive API](../../assets/screenshots/googledrive/5.png)

1. When API is enabled, you will see a warning message on the dashboard, which will say: `To use this API, you may need credentials. Click 'Create credentials' to get started.`. Click the `CREATE CREDENTIALS` next to it.

    ![CREATE CREDENTIALS](../../assets/screenshots/googledrive/6.png)

1. There will be a wizard asking for the credential type. Select `Google Drive API` and `User Data` and press `NEXT`.

    ![Credential Type](../../assets/screenshots/googledrive/7.png)

1. The second Wizard screen is `OAuth consent screen`, fill in the necessary parts and Click `SAVE AND CONTINUE`.

    ![OAuth consent screen](../../assets/screenshots/googledrive/8.png)

1. The third Wizard screen is `Scopes`, skip this screen by simply pressing `SAVE AND CONTINUE`.

1. The fourth screen is `OAuth Client ID`. Select Desktop App as `Application type` and the `Name` field can remain as it is, then click the `CREATE` button.

    ![OAuth Client ID](../../assets/screenshots/googledrive/9.png)

1. On the final screen, Google will show your Client ID and give an option to download application credentials in JSON format, which is what we are looking for. Press `DOWNLOAD`. We don’t need the Client ID itself.

    ![Download application credentials](../../assets/screenshots/googledrive/10.png)

    This is an example of what you will find in the downloaded JSON file.
    ```json
    {
      "installed": {
        "client_id": "{CLIENT_ID}.apps.googleusercontent.com",
        "project_id": "{PROJECT_ID}",
        "auth_uri": "https://accounts.google.com/o/oauth2/auth",
        "token_uri": "https://oauth2.googleapis.com/token",
        "auth_provider_x509_cert_url": "https://www.googleapis.com/oauth2/v1/certs",
        "client_secret": "{CLIENT_SECRET}",
        "redirect_uris": ["urn:ietf:wg:oauth:2.0:oob", "http://localhost"]
      }
    }
    ```

1. After downloading the file, press `OK`, this will take you back to the project's credentials page. Select `OAuth consent screen` from the side menu and under the `Publishing status` section, press the `PUBLISH APP` button. We’re done here.

    ![PUBLISH APP](../../assets/screenshots/googledrive/11.png)

### Creating Google Drive storage

```console
$ wl template create googledrive --inline --container YOUR_CONTAINER_NAME --credentials 'CONTENT_OF_CLIENT_CONFIG_JSON_IN_BETWEEN_SINGLE_QUOTE'
```

### Mounting

```console
$ wl start
$ wl c mount YOUR_CONTAINER_NAME
```

### Google Drive documentation

- [Google Developer Console](https://console.developers.google.com/)
- [Google Drive Python API repository](https://github.com/googleapis/google-api-python-client)
- [Google Drive API documentation](https://developers.google.com/drive/api/v3/about-sdk)
