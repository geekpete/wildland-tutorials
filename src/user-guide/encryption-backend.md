# Encryption


## What?

Encryption backend is the way Wildland deals with the problem of storage providers or alphabet-soup organizations snooping on your data. It encrypts file contents, file names and directory names on your computer, and stores them in encrypted form wherever it has been instructed to. Your Internet Service Provider and your storage providers will not see the contents of your data.

## Prerequisites

Please ensure that [`gocryptfs`][gocryptfs] is installed. Alternatively [`encfs`][encfs] can be used, but it's not recommended. Both are installed as a part of the Wildland-Client Docker image.

## How?

You can have any type of storage backend for encryption. For this tutorial let's create a S3-backed container for ciphertext, and a container for summer vacation photos with encrypted backend. Since there is not much value in ciphertext polluting the Wildland mount point, we will use the `.uuid` path when referencing the S3-backed container. To retrieve it, the `wl container dump` command can be used. Although this tutorial uses S3 as an example, the same procedure can be applied to other backends as well. 

```console
$ wl container create s3-photos
Created: /home/user/.config/wildland/containers/s3-photos.container.yaml

$ wl container dump s3-photos
object: container
owner: '0x588e74228179f56a12bc5b3501678726cc7f5226ce27c416e139c49630bf2e33'
paths:
- /.uuid/0fb3bc88-87a6-44d8-be27-33e18bf143f2
backends:
  storage: []
title: null
categories: []
version: '1'

$ wl storage create s3 \
    --container s3-photos \
    --s3-url s3://thenameofyourbucket/and/a/path/ \
    --access-key AKIAXXXXXXXXXXP7I \
    --secret-key btHxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxoR

$ wl container create greece-vacation \
    --category /photos \
    --category /time/2019/07 \
    --category /persons/tim \
    --category /persons/janet \
    --category /persons/luke
```

Next, encrypted backend can be added to the `greece-vacation` container. It will point to the `s3-photos` container, which will be referenced via its `/.uuid/` path, that has been retrieved above.

```console
$ wl storage create encrypted \
    --container greece-vacation \
    --reference-container-url wildland::/.uuid/0fb3bc88-87a6-44d8-be27-33e18bf143f2: \
    --engine gocryptfs
```

Please **note** the semicolon ':' at the end of the **url**. If everything went well, the container can be mounted as follows:

```console
$ wl start
$ wl container mount greece-vacation
```

_NB: The referenced container will be mounted automatically._

## Questions and answers

- **Q:** How does encryption backend store encryption keys?

  **A:** They are stored in the storage manifest. You can examine them via the `wl container dump` command. You don't need to do anything with them though – they are managed automatically.

- **Q:** I've revealed my encryption keys to someone. What should I do?

  **A:** If you want to prevent that person from accessing your data in the future, you need to create a new container with encrypted backend and move your files there.

- **Q:** I want to give access to the contents of a container to someone else. Does the fact that encrypted backend is being used complicate the sharing process?

  **A:** No, it does not. Please note though, that sharing reveals the encryption keys to the person you are sharing them with.

## Encryption engines

Encrypted backend uses, alternatively, `gocryptfs` or `encfs`. This choice is controlled with the `--engine` option. Gocryptfs is the recommended option. Please note that neither `gocryptfs` nor `encfs` are capable of protecting the integrity of your data. A motivated and (very) skillful attacker with access to the ciphertext can potentially carry out remote code execution on your machine!

[gocryptfs]: https://github.com/rfjakob/gocryptfs
[encfs]: https://github.com/vgough/encfs
