# Public Forests

With the initial relase of the [Wildland client](https://gitlab.com/wildland/wildland-client) we are opening up, for public read-only access, two Wildland forests (a forest is a namespace built around one user identity):

- **The Ariadne Forest** - (a reference to [Greek mythology](https://en.wikipedia.org/wiki/Ariadne)), which is a small forest containing bridges to other forests, specifically to the Pandora forest that's mentioned below, and some other, internal forests, which are encrypted and not exposed for public use.

- **The Pandora Forest** (a reference to the [Avatar](<https://en.wikipedia.org/wiki/Avatar_(2009_film)>) movie), a repository of various documents, which [Golem Foundation](https://golem.foundation/) has been using internally to help with Wildland's development.

To access the Pandora forest first you have to import it:

```console
$ wl user import --path /mydirs/ariadne https://ariadne.wildland.io
```

Next step is to mount it:

```console
$ wl forest mount :/mydirs/ariadne:
```

Now, you should be able to see the content of the Ariadne forest inside the directory represented by the bridge:

```console
$ tree -F ~/wildland/mydirs/
/home/user/wildland/mydirs/
└── ariadne:/
    └── forests/
        └── pandora:/
            └── WILDLAND-FOREST.txt
```

You can now mount the Pandora forest as well:

```console
$ wl forest mount :/mydirs/ariadne:/forests/pandora:
```

For more information about forests, refer to the [Quick Start Guide][quick-start].

## Notice

Pandora is a repository of memos, reports, and documents, used internally by the [Golem Foundation](https://golem.foundation/). You may find them useful, but please bear in mind that all such materials remain property of Golem Foundation or their respective owners. You cannot reproduce, duplicate, copy or otherwise disclose, distribute, transmit or disseminate any of these materials or any portion thereof in any medium.

For more details, please see Terms of Use which you will find in Pandora.

[quick-start]: ./quick-start.md#accessing-existing-forests
