# Group Users

Every Wildland user may contain multiple references to other users and therefore become a *group user*. These group users are especially useful when wanting to share content with a group of users and/or wanting to allow other users to sign manifests (e.g. containers) in the group user's name.

Let's assume we have two users, *Yatima* and *Inoshiro*.

```console
yatima@wildland-client:~$ wl user create yatima
Generated key: 0x78d9dc1cee7220437e7b17184eda06258d8b6de740a505dfddc47ff8d7b981fa
No path specified, using: /users/yatima
Created: /home/user/.config/wildland/users/yatima.user.yaml
Using 0x78d9dc1cee7220437e7b17184eda06258d8b6de740a505dfddc47ff8d7b981fa as @default
Using 0x78d9dc1cee7220437e7b17184eda06258d8b6de740a505dfddc47ff8d7b981fa as @default-owner
Adding 0x78d9dc1cee7220437e7b17184eda06258d8b6de740a505dfddc47ff8d7b981fa to local owners
```

```console
inoshiro@wildland-client:~$ wl user create inoshiro
Generated key: 0x6f6310b6850cf2136c845ec7a87735bc3dda07112145b47d11f3031be2359fd7
No path specified, using: /users/inoshiro
Created: /home/user/.config/wildland/users/inoshiro.user.yaml
Using 0x6f6310b6850cf2136c845ec7a87735bc3dda07112145b47d11f3031be2359fd7 as @default
Using 0x6f6310b6850cf2136c845ec7a87735bc3dda07112145b47d11f3031be2359fd7 as @default-owner
Adding 0x6f6310b6850cf2136c845ec7a87735bc3dda07112145b47d11f3031be2359fd7 to local owners
```

Additionally, we create a group user called *Office*:

```console
office@wildland-client:~$ wl user create office
Generated key: 0x10f613dfd929a86e86ffe8cbbbd6bd4f3751ddc411d36408d7d23eac36b28ff2
No path specified, using: /users/office
Created: /home/user/.config/wildland/users/office.user.yaml
Using 0x10f613dfd929a86e86ffe8cbbbd6bd4f3751ddc411d36408d7d23eac36b28ff2 as @default
Using 0x10f613dfd929a86e86ffe8cbbbd6bd4f3751ddc411d36408d7d23eac36b28ff2 as @default-owner
Adding 0x10f613dfd929a86e86ffe8cbbbd6bd4f3751ddc411d36408d7d23eac36b28ff2 to local owners

office@wildland-client:~$ wl user import ~/downloads/yatima.user.yaml
Created: /home/user/.config/wildland/users/yatima.user.yaml
Created: /home/user/.config/wildland/bridges/yatima.bridge.yaml

office@wildland-client:~$ wl user import ~/downloads/inoshiro.user.yaml
Created: /home/user/.config/wildland/users/inoshiro.user.yaml
Created: /home/user/.config/wildland/bridges/inoshiro.bridge.yaml
```

Add *Yatima* and *Inoshiro* to *Office*:

```console
office@wildland-client:~$ wl user modify add-pubkey --user yatima --user inoshiro office
Pubkeys found in [yatima]:
  RWTE+IfGvuDCmSpSAFs8volo5W+IWKaJA4l7gOS+aohnWUNZ2zCfo1RJ57dqwwpNzsp5RAj7EaaVf7B59IyyNpIg
Pubkeys found in [inoshiro]:
  RWQXf5ileNA3CNps++nyaQ7+XjMxoSDET5cbTHiAvZcGhLVy2AwTNvoI7Hcj+IAhzHpH7vHE5JH7d3PMRSiGcCN1
Saved: /home/user/.config/wildland/users/office.user.yaml
```

The *Office* user now contains three public keys:

```console
office@wildland-client:~$ wl user dump office
object: user
owner: '0x10f613dfd929a86e86ffe8cbbbd6bd4f3751ddc411d36408d7d23eac36b28ff2'
paths:
- /users/office
manifests-catalog: []
pubkeys:
- RWRnr+NkBoQit+kn7l/1RHDjWO4IKfPyxhaROpPW4F7m1UoLSn7Z1q1x7TbK0R3Dc/buKMs/MUAqPbMNXk0T7lEn
- RWTE+IfGvuDCmSpSAFs8volo5W+IWKaJA4l7gOS+aohnWUNZ2zCfo1RJ57dqwwpNzsp5RAj7EaaVf7B59IyyNpIg
- RWQXf5ileNA3CNps++nyaQ7+XjMxoSDET5cbTHiAvZcGhLVy2AwTNvoI7Hcj+IAhzHpH7vHE5JH7d3PMRSiGcCN1
version: '1'
```

Note that the *Office* user now contains three pubkeys with the first one being the *Office* user's pubkey, while the remaining ones are *Yatima*'s and *Inoshiro*'s respectively.

Now each resource where `access` is set to *Office* will be encrypted using all three pubkeys, allowing for *Yatima* and *Inoshiro* to read manifest contents encrypted with the *Office* user.

```console
office@wildland-client:~$ wl c create --access office access-test
Created: /home/user/.config/wildland/containers/access-test.container.yaml

office@wildland-client:~$ cat ~/.config/wildland/containers/access-test.container.yaml
signature: |
0x10f613dfd929a86e86ffe8cbbbd6bd4f3751ddc411d36408d7d23eac36b28ff2:KgeMIh5KsynCCldzjVKudGGufZylwIdjYD4RP8CZKRR2O9jC01fCGjQ1x5guVXRaToXlCucxo/jqKNsIClE5AA==
---
encrypted:
  encrypted-data: vkYcpRmMd4y0JYslKs0iH6JAWfo9khYsD17TSmiKV24IB28QIgF4FvW2BJBOP+w8FiQ7KbuTyucbCttahqv15C7eOCKS4VQacsecykyyCjP9jKlqTS3j2Mr1McqSPs7HJFl9FeXSD//9Hg6mTOT3Ytg7P4ombmnQSzA3OTXMKs3D1mwEgOr9vo3saE7ikLTuZAbNQm4gU+3JzUfzZ9kxOOTV1JmbV05Of8HTwNBS5kvl5MjBR9MDB/zmSku7r9ajWz45kaotJehlGGQrS+N5jcR3gOZWPhRSGw65a2HgRIPHuGcKqW3U5rA04gr6GA3tTwPbTQ3kLXRuvi4Iziv0EMtB3GHolODbWLYXA7eiejXVFMAfFbB94Wjf7dFsmhFxjnksjHDDqg3vj2bJjNyVkwxrXAaIlMdgaydekVOr8B/0DCLebWT29k4qfo1qcfxA
  encrypted-keys:
  - GLivFv/5cDIvsdDiybIWPjJOzwv/9a44cJkSEOXU9DuHLRUWZU29EnqT80q7pwsggS7L5N20Q+En+ACmYwV892sJLdDSXWhF8iQvr+dR+XA=
  - XBjNHqgZvhVal/GzRs16XV1DnSSCXAzLsS9Ds40v/z8Xj+azrU1LotTRbVgwtfSAt+3tb4T+4NreaDGI6sS8fVc4Zn/qY/NAhSyPskuz5B8=
  - zvpd1mtxLzJUQ+ZVlKO5Ap51+6papAe6NS4tjrvGaR16Wv9pBd4stXLmwdH+8zu6+2IJAxpVZWMtzjrgCPIMTXBVkh/BME+ao3ugD96J4PU=
```

*Yatima*'s machine:
```console
yatima@wildland-client:~$ wl user import ~/downloads/office.user.yaml
Created: /home/user/.config/wildland/users/office.user.yaml
Created: /home/user/.config/wildland/bridges/office.bridge.yaml

yatima@wildland-client:~$ wl dump ~/downloads/access-test.container.yaml
object: container
owner: '0x10f613dfd929a86e86ffe8cbbbd6bd4f3751ddc411d36408d7d23eac36b28ff2'
paths:
- /.uuid/8020e58c-c926-422d-bd0c-05b541c54f1d
backends:
  storage: []
title: null
categories: []
version: '1'
access:
- user: '0x10f613dfd929a86e86ffe8cbbbd6bd4f3751ddc411d36408d7d23eac36b28ff2'
```

As you can see, *Yatima* was able to read the encrypted container created with the `--access office` parameter. Note that both the container's owner and the container's access is set to `0x10f613dfd929a86e86ffe8cbbbd6bd4f3751ddc411d36408d7d23eac36b28ff2` which is *Office*'s signature.

What's more, *Yatima* will now be able to re-sign (or edit) the container, even though she is not directly an owner of that container, but a part of the group user that owns the container.

```console
yatima@wildland-client:~$ wl sign -i ~/downloads/access-test.container.yaml
Saving: /home/user/downloads/access-test.container.yaml

yatima@wildland-client:~$ cat ~/downloads/access-test.container.yaml
signature: |
0x78d9dc1cee7220437e7b17184eda06258d8b6de740a505dfddc47ff8d7b981fa:TdH7t245wIEGWgxuQvGlHa4Q1RfosBh5FDcuRvB62rArm+D6vx9ar3W8AGUWO1bNhKr3AhhdjcxIZKtzs38iAg==
---
encrypted:
  encrypted-data: N0FiwldIkiBFFXVkwvDnzNl7npcR5LvfNp1Knz3x7XYNFtjZwnLVKZAYEActcOJ1EFTdYIfF80vNy5z8N0YJ+FpIV2mFG0ko2g0F4rWqtLQFXq2ytfU/w7skggwlZ4s6xB23zUzpCMGSZ9HYM6ZNGIfgUkEyUwWdoWNhO2I8A17N5+APP1G/E7X6JrRWs0ipvg9gjvciIEmt8CxBk2h7JLYWUvd4z90vvXhWaRlh56LNz3sYyLgQDus79RNtfxzgWL2WEMjAXlI1WFI6JqQFIKOg7KVEtxylDM/1U3/9GAUrN8kozkdNKJAoyoRwpK/Tg8wgROT/yWhH7VdhcLj2ZkQEUSBmSi8ipD57V645qhXdkVVlVjEBBKkDVDgsXV0yWnMOJIdLKX3lDSTplllPGexMWzR7XrSnQFb8UQ5AwUj5OY5UjA02t3vXBs/ASgo6
  encrypted-keys:
  - LwfzhAxJeevwaeyPsxLNz0kCntSgeCNthwSI9/SNOi0XzOWrnbbUDe9bSyRjZ6vV7rWRlx6MnZEtPEbjcovShBdgj7traJtfa2bYvh81cLk=
  - vEteiokS9LEh6WM4gVJu3LeLQy28OY5iRs8fPWO+jg3XhO6bCTlgk40a1Btc7u8059kF5dJFqvnBuiJHrI50f2umPaaMJH0kesRMi6wMAL0=
  - CpZU0WUsfVTc/2TCZFJfVYLRNeiYghptwvYzlzIWTDUMhvR6LPVw3QkhEmatFhF6tD5VPCZcfo08poxz7wO7Vvr0lw/lcgXJTQ+cFyopbss=
```

## Encrypting, signing and ownership in a group user

To digitally sign a manifest as a specific user, you need that user's private key. If so, how was *Yatima* able to sign a manifest owned by *Office*? It was possible because in fact *Yatima* didn't sign the manifest as *Office* but as *Yatima*.

Note that after *Yatima* re-signed the container, the signature shows `0x78...` which is *Yatima*'s signature (even though the container's owner is still *Office*). That is because *Yatima* cannot sign a manifest as if she were *Office* - that would allow any group user to impersonate the group user itself, something that could be easily abused. The container is therefore signed by *Yatima* but other users (here only *Inoshiro*), will consider this manifest as valid because *Yatima*'s pubkey is part of the *Office* user, who owns the container.

If a user is a group user (i.e. has more than 1 pubkey in it's `pubkeys`), Wildland will recognize any manifest owned by the group user, but signed by any of the group user's members, as a valid manifest.

Encrypting a manifest, based on the `access` field, uses a non-deterministic and asymmetric encryption algorithm which is not part of the manifest validation and/or ownership. It merely tells Wildland which public keys should be used to encrypt the contents. For that very reason, when a group user is specified in the `access` field, Wildland will use all `pubkeys` of that user to encrypt the manifest contents.
