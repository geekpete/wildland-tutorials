## Publishing containers in the Pandora Forest

### What?

This document describes how you can publish your local containers (most likely having delegate storages) to Pandora Forest. Pandora Forest itself must not hold any data (documents, images, audio) but serve as a "relay" to other Forests, hence Pandora Forest consists of Manifest files only.

### Why?

Your files will eventually reside in your home storage but you need to allow Pandora users to access these files using Wildland paths as well as categorize them.

### How?

_Note: This section assumes you have already imported Pandora user into your local Wildland_

1. Create delegate storage template

    ```console
    $ wl storage-template create delegate pandora \
        --reference-container-url wildland:0x1ea3909882be658d0ab69a822f7c923d12454ec024f4d8dd8f7113465167fcbe:/.infra:/home/<yourname>:
    ```

    If you don’t yet have a home dir (and bucket) on Pandora, please send your user manifest to michal@wildland.io and he will happily create one for you.

1. Create a "micro-container" for exposing this document in our Pandora forest:

    ```console
    $ wl c create knowledge-sharing \
        --no-encrypt-manifest \
        --owner pandora \
        --title "Knowledge Sharing in Wildland Project" \
        --category /docs \
        --category /ops \
        --category /persons/joanna \
        --template pandora
    ```

1. Mount (or re-mount) Pandora Forest:

    ```console
    $ wl f mount :/forests/kartoteka:/forests/pandora:
    ```

1. Copy your artefacts into one of the paths you created (e.g. `mnt/forests/pandora:/persons/joanna/Knowledge Sharing in Wildland Project` ). And yes, again, you can now use Finder to do that!


Check how well it matches the conventions in the forest. Does it look good? Think if others will be able to easily find your piece of info given the paths/categories you just used.

In case you think you want to correct it, please remove your container and start all over again (skip step 1). You can use the up-arrow in the console to bring back the recent commands, so you just need to edit them. Those lucky ones who use Sublime Text, can edit manifest in-place, thanks to the wonder Wildland plugins, which Michał wrote (they are on our GitLab). There is also an option to edit the manifest via `wl c edit knowledge-sharing` command, but this will spawn VIM in the console, so use with caution if you’re not a vim-type of person. You can also edit in an editr of your choice and then use the `wl c sign -i knowledge-sharing` command to re-sign it. But use with caution!

***

Keep in mind that Pandora infra relies on cloudfront front-end cache, thus you might have to wait a while (usually not longer than a minute) after you publish a container but before you'll be able to mount it through Pandora forest.

***

A note on publishing reports — in addition to publishing it under the `/reports` please also think about what other topics/areas it relates to or covers and include these in the container description. A few months from now, when someone will want to find all the info about subcontainers, say, they would be able to go directly into `/arch/subcontainers/` tree, rather then search through all the reports, from all the people...

As an example, this is how I would categorize the last ITL report:

```yaml
title: "ITL report 2021-01-22”
categories:
- /reports
- /persons/marmarek
- /backends/local
- /arch/directories/bridges
- /arch/manifests/format
- /timeline/2021/01/22
```

BTW, you can see the timeline category also — I think that, at least for reports, we should actually be adding these manually. Later we might also gain an auto-timelining proxy backend.
