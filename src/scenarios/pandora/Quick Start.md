## Quick start

### What?

A short, step-by-step tutorial on how to get Wildland running with Pandora Forest. Note that this Quick Start will not
get into details on _why_ certain step is required and/or _what_ is the expected outcome but merely how to access
Pandora files as quickly as possible.

### How?

1. Get `https://gitlab.com/wildland/wildland-client` repository.

   _Note: Gitlab allows you to download a zip file instead of cloning a repository using `git` tool. However, in order to
   keep the project up to date seamlessly, it is advised to get used to the git command-line tool_.

   Having your `git` tool setup, run the following command:

   ```console
   git clone https://gitlab.com/wildland/wildland-client.git
   ```

1. Navigate to the project you just cloned:

   ```console
   $ cd wildland-client/docker
   ```

1. Build the docker image:

   ```console
   $ docker-compose build wildland-client
   ```

1. Run the container and jump into it:

   ```console
   $ docker-compose run --service-ports wildland-client
   ```

#### The following commands you will run from within the Docker container

_NB: If you already have a user, you may skip to step 7. To see users list run `wl user ls`_.

5. Create a Wildland user and note the output path (i.e. `Created: /home/user/….user.yaml`)

   ```console
   $ wl user create <your_name>
   ```

1. Copy the user file's content and send it to `michal@wildland.io` so that he can add you to Pandora.

   ```console
   $ cat /home/user/….user.yaml
   ```

#### Await confirmation that you are added to Pandora.

_NB: If you already have imported Pandora user, you should skip to step 8. To see users list run `wl user ls`_.

7. Import Pandora user (skip if you already have Pandora user imported).

   ```console
   $ wl user im --path /users/pandora https://a0f6eb0b-cb70-0add-bbf4-5901da83ecdd.s3.eu-north-1.amazonaws.com/manifests/pandora.yaml
   ```

1. Mount pandora

   ```console
   $ wl forest mount :/users/pandora:
   ```

1. Pandora is now exposed via webdav on `http://127.0.0.1:8080`. If you use macOS, you can mount it in Finder
   using `⌘+k` and pasting `http://127.0.0.1:8080` (no authentication required).

### Troubleshooting

There are three steps to take before you report a "doesn't work" issue:

1. Refresh user manifests:

   ```console
   $ wl user refresh
   ```

1. Make sure you have only one instance of your user i.e., there is no duplicated user with your name and the
   signature (`0x...`) of your user matches the `@default` in `/home/user/.config/wildland/config.yaml`.

1. Exit the Docker container and rebuild the Docker images from scratch. Then return to step 4.

   ```console
   $ docker-compose build --no-cache wildland-client-ci wildland-client
   ```

If mounting pandora still doesn't work, run the mount command with verbose flag and send the output together with
contents of `/tmp/wlfuse.log` to `michal@wildland.io`.

```console
wl -vv forest mount :/users/pandora:
```
