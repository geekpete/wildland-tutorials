# Cafe Admin HOWTO

`TODO`: verify this procedure on a new forest from scratch before publishing!

This HOWTO expands the Cafe-HOWTO and contains instructions on how to create and curate a Cafe-like forest. Please read the Cafe-HOWTO first.

## Creating the forest

1. Create a group user who will own this forest:

    ```console
    $ wl user create cafe
    ```

1. Decide on the storage used for _manifest catalog container_ (note this doesn't need to be the same storage as one used for actual data-holding containers stored within this forest) and create a template for this storage, e.g.:

    ```console
    $ wl storage-template create s3 cafe-catalog \
        --access-key ... --secret-key ... \
        --s3-url s3://... \
        --access cafe
    ```

    Note the `--access cafe` will cause any storage manifest created out of this template to be encrypted for the cafe group user. If the forest is to also be shared publicly, but with r/o permissions, add another template for r/o access:

    ```console
    $ wl storage-template create s3 cafe-catalog \
        --access-key <READ_ONLY_KEY> --secret-key <READ_ONLY_SECRET> \
        --s3-url s3://... \
        --access '*'
    ```

    Make sure that r/w template precedes the r/o template!

1. Finally create the forest:

    ```console
    $ wl forest create cafe cafe-catalog
    ```

## Adding/removing users

New users should be added to the `cafe` group user:

**WARNING:** Make sure the pubkey you pass is correct. Otherwise you risk your user manifest being irreperably damaged (ref. #350)

```console
$ wl u modify add-pubkey --pubkey <pubkey>(starts with RW...) cafe
```

To remove the user:

```console
$ wl u modify del-pubkey --pubkey <pubkey>(starts with RW...) cafe
```

Afterwards, all the manifests within the forest should be re-encrypted. Right now this needs to be done manually (but see https://gitlab.com/wildland/wildland-client/-/issues/377):

```console
$ find . -name '*.yaml' -exec wl sign -i {} \;
```
