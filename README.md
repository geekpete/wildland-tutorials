# Wildland Tutorials

This repo is for "raw" content to be used in various Wildland tutorials.

It's concerned with **content** (texts and essential diagrams) rather than with form (actual formatting of posts, slides, etc). It is expected that this "raw content" from here would be later cherry-picked to create the actual websites, slides, posts, likely in other repos.

## Conventions


### Text

All texts should be in Markdown format. This is to allow easy merging of several texts.

All texts should use gender-neutral grammatical forms, i.e. they/them, except for occasionally examples with named characters (e.g. Alice and Bob).

Also personal grammar constructions should be avoided ("I show you now how to...") and more neutral forms preferred ("This can be done in the following way...").

Each document should be written with an assumption that the reader might not immediately know the context and so should contain at least one intro paragraph explaining the purpose and providing any necessary the context (at the very least to allow the reader to understand if this document is applicable/interesting to them).

Inline-ing command-line examples, e.g. `wl container create`, should be avoided and instead the more distinctive block-form should be used:

```console
$ wl container create
```

### Diagrams

There is currently no agreement for the diagrams format. Most will likely be created by hand in the Concepts iOS app or OmniGraffle and committed here in source vector format and one or more ready-for-consumption, platform-independent, open formats such as: PDFs or PNGs. The availability of the source format for diagrams is very important, since we might want to be able to generate light- and dark-scheme compatible versions of the diagrams.

There is yet no agreement for the aesthetics conventions of the diagrams, these are expected to emerge organically with time.

## Target audience

Computer-literate user, who understands what a file, directory and file manager is.

Some more advanced material might target **power users**, i.e. those who can write own shell scripts and create and run own Dockers.

Note that these tutorials should not be targeted at prospective Wildland developers (contributors), since this kind of specialized documentation should be found elsewhere (e.g. the "Plugin Writer's HOWTO" is part of the `wildland-client` repo).

### Copyright

© 2021 Golem Foundation, Zug, Switzerland
